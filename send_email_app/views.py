# Create your views here.
import datetime,time
from django.conf import settings
from django.http import Http404,HttpResponse,HttpRequest
from django.shortcuts import render
from django.core.mail import send_mail, BadHeaderError,EmailMessage
from smtplib import SMTPRecipientsRefused
from django import forms
from send_email_app.models import Store_emails

class ContactForm(forms.Form):
    subject = forms.CharField()
    email = forms.EmailField(required=True)
    message = forms.CharField(widget=forms.Textarea)
    attachfile = forms.FileField(label='PDF Attachment', widget = forms.FileInput,    required = False)



def mail_send(request):
    subject = str(request.GET['subject'])
    body = str(request.GET['body'])
    to = str(request.GET['to'])
    to = to.split(',')
    if subject and body and to:
        try:
            if send_mail(subject,body,'prasanth.potnuru249@gmail.com',to) == 1:
                now = datetime.datetime.now()
                return HttpResponse('email has sent successfully')
        except BadHeaderError:
            return HttpResponse('Invalid header found.')
        return HttpResponse('Hi')
    else:
        # In reality we'd use a form class
        # to get proper validation errors.
        return HttpResponse('Make sure all fields are entered and valid.')
    
def contact(request):
    try:
        if request.method == 'POST':
            form = ContactForm(request.POST,request.FILES)
            if form.is_valid():
                cd = form.cleaned_data                
                subject = cd['subject']
                message = cd['message']
                to = [cd['email']]
                email = EmailMessage(subject,
                                     message,
                                     'prasanth.potnuru249@gmail.com',
                                     to,)
                if cd['attachfile'] != None:
                    attach1 = request.FILES['attachfile']
                    
                    if attach1.content_type == 'application/pdf' :
                        email.attach(attach1.name, attach1.read(), attach1.content_type)
                    else:                        
                        form.errors['attachfile'] = ['please attach pdf file']
                        return render(request, 'contact_form.html', {'form':form})
                if email.send() == 1:
                    localtime = datetime.datetime.today().strftime('%Y-%m-%d')
                    store_data = Store_emails(email = cd['email'], send_date = localtime)
                    store_data.save()
                    return HttpResponse('Email send successfully')
        else:
            form = ContactForm()
        return render(request, 'contact_form.html', {'form':
    form})
    except BadHeaderError:
            return HttpResponse('Invalid header found.')

