from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Store_emails(models.Model):
    email = models.EmailField()
    send_date = models.DateField()
